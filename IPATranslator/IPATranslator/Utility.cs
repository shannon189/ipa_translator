﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPATranslator
{
    class Utility
    {
        public static void PauseBeforeContinuing()
        {
            Console.WriteLine("\nPress a key to continue.");
            Console.ReadKey();
        }

        public static string GetString(string message = "Enter the appropriate string")
        {
            string output = "";
            do
            {
                Console.Write(message + ": ");
                output = Console.ReadLine();
            } while (string.IsNullOrWhiteSpace(output));
            return output;
        }

        public static string GetStringMaxWords(int maxWords,  string message = "Enter the appropriate string")
        {
            string output = "";
            bool overWordLimit;
            do
            {
                Console.Write(message + ": ");
                output = Console.ReadLine().ToLower().Trim();
                int count = 0;
                for(int i = 0; i < output.Length; i++)
                {
                    if(output[i] == ' ')
                    {
                        count++;
                    }
                }

                if(count < maxWords)
                {
                    overWordLimit = false;
                } else
                {
                    overWordLimit = true;
                }

            } while (string.IsNullOrWhiteSpace(output) || overWordLimit);
            return output;
        }

        public static string GetStringMinWords(int minWords, string message = "Enter the appropriate string")
        {
            string output = "";
            bool overWordLimit;
            do
            {
                Console.Write(message + ": ");
                output = Console.ReadLine().Trim();
                int count = 0;
                for (int i = 0; i < output.Length; i++)
                {
                    if (output[i] == ' ')
                    {
                        count++;
                    }
                }

                if (count > minWords)
                {
                    overWordLimit = false;
                }
                else
                {
                    overWordLimit = true;
                }

            } while (string.IsNullOrWhiteSpace(output) || overWordLimit);
            return output;
        }

        public static int GetInt(string message = "Enter the appropriate integer")
        {
            int output = 0;
            string outputString = "";
            do
            {
                Console.Write(message + ": ");
                outputString = Console.ReadLine();
            } while (!int.TryParse(outputString, out output));

            return output;
        }

        public static int GetInt(int min, int max, string message = "Enter the appropriate integer")
        {
            int output = 0;
            string outputString = "";
            do
            {
                Console.Write(message + ": ");
                outputString = Console.ReadLine();
            } while (!int.TryParse(outputString, out output) || output > max || output < min);

            return output;
        }
    }
}
